# Copyright 2023 <omicron.me@protonmail.com>
# Copyright 2023 <andreasruden91@gmail.com>
#
# This file is part of d2warehouse.
#
# d2warehouse is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# d2warehouse is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Mercator. If not, see <https://www.gnu.org/licenses/>.
from bitarray import bitarray
from d2warehouse.parser import parse_item
import d2warehouse.huffman as huffman

test_items = [
    (
        "Leather armor (16 armor, 24/24 durability)",
        "102080000500f40e2f087bf2b426041ac0c0f01f",
    ),
    (
        "Ethereal Leather Armor (24 armor, 13/13 durability)",
        "1000c0000500f40e2f9c8627a70404226868f01f",
    ),
    (
        "Unidentified Ethereal Magic Leather armor (27 armor, 13/13 dura)",
        "0000c0000500f40e2f08a37fd61388470040091a1a4088f01f",
    ),
    (
        "^ The same item but identified (+17% ED, Sturdy prefix",
        "1000c0000500f40e2f08a37fd61388470040091a1a4088f01f",
    ),
    ("minor healing potion", "1000a008050014cf4f00"),
    ("beginner minor healing potion", "1020a200050014cf4f00"),
    ("minor mana potion", "1000a0000500d4ce4f00"),
    ("minor rejuvenation potion", "1000a0000500f4ec28"),
    ("stamina potion", "1000a0000500743729"),
    ("antidote potion", "1000a0000500143529"),
    ("thawing potion", "1000a000050014580a"),
    ("beginner scroll of identify", "1000a2000500f44722"),
    ("scroll of identify", "1000a0000500f44722"),
]


def txtbits(bits: bitarray) -> str:
    txt = "".join(str(b) for b in bits)
    grouped = [txt[i : i + 8] for i in range(0, len(txt), 8)]
    return " ".join(grouped)


def main():
    for descr, hex in test_items:
        print(descr)
        print(hex)
        raw = bytes.fromhex(hex)
        bits = bitarray(endian="little")
        bits.frombytes(raw)
        print(txtbits(bits))
        _, item = parse_item(raw)
        print(f"kind: {item.kind}")
        print(f"simple: {item.is_simple}")
        print(f"identified: {item.is_identified}")
        print(f"beginner: {item.is_beginner}")
        print("--")
    print(huffman.encode("hp1"))
    print(huffman.encode("mp1"))
    print(huffman.encode("rvs"))
