import unittest
from d2warehouse.parser import parse_stash


class ParseStashTest(unittest.TestCase):
    def test_empty(self):
        # empty stash with 3 tabs and some gold
        data = bytes.fromhex(
            "55aa55aa0100000063000000a40100004400000000000000000000000000"
            "000000000000000000000000000000000000000000000000000000000000"
            "000000004a4d000055aa55aa0100000063000000391b0000440000000000"
            "000000000000000000000000000000000000000000000000000000000000"
            "0000000000000000000000004a4d000055aa55aa01000000630000003905"
            "000044000000000000000000000000000000000000000000000000000000"
            "00000000000000000000000000000000000000004a4d0000"
        )
        stash = parse_stash(data)
        self.assertEqual(len(stash.tabs), 3)
        gold = tuple(tab.gold for tab in stash.tabs)
        self.assertEqual(gold, (420, 6969, 1337))
