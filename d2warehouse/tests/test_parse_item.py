import unittest
from d2warehouse.parser import parse_item
from d2warehouse.item import Quality, lookup_runeword


class ParseItemTest(unittest.TestCase):
    def test_gem(self):
        # chipped diamond
        data = bytes.fromhex("1000a0000524740910")
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertEqual(item.code, "gcw")
        self.assertEqual(item.pos_x, 9)
        self.assertEqual(item.pos_y, 0)
        self.assertTrue(item.is_simple)

    def test_buckler(self):
        # white buckler
        data = bytes.fromhex("10008000050054a1083f57a9190c01041818fc07")
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertEqual(item.code, "buc")
        self.assertEqual(item.pos_x, 0)
        self.assertEqual(item.pos_y, 0)
        self.assertEqual(item.quality, Quality.NORMAL)
        self.assertFalse(item.is_simple)
        self.assertEqual(item.lvl, 12)

    def test_rune(self):
        # tal rune
        data = bytes.fromhex("1000a0000564f67cfb00")
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertEqual(item.code, "r07")
        self.assertEqual(item.pos_x, 9)
        self.assertEqual(item.pos_y, 9)
        self.assertTrue(item.is_simple)

    def test_rare_a(self):
        # random rare armor
        data = bytes.fromhex(
            "10008000050014df17c64083c30b866d534c958c39d15b42c0c1e1c08815cd9c04f80dff01"
        )
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertEqual(item.code, "hla")
        self.assertEqual(item.pos_x, 0)
        self.assertEqual(item.pos_y, 0)
        self.assertEqual(item.quality, Quality.RARE)
        self.assertFalse(item.is_simple)
        self.assertEqual(item.lvl, 5)
        self.assertEqual(len(item.stats), 4)
        self.assertEqual(item.defense, 23)
        self.assertEqual(item.durability, 28)
        self.assertEqual(item.max_durability, 28)

    def test_tp_tome(self):
        data = bytes.fromhex("100080000500d4a814daf7275a180240e03f")
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertEqual(item.code, "tbk")
        self.assertEqual(item.quantity, 4)

    def test_superior_sockets(self):
        # superior armor with empty sockets
        data = bytes.fromhex("10088000050014df175043b1b90cc38d80e3834070b004f41f")
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertEqual(item.quality, Quality.HIGH)
        self.assertEqual(len(item.stats), 2)
        self.assertEqual(len(item.sockets), 2)

    def test_ed_max(self):
        # test bugfix for https://gitlab.com/omicron-oss/d2warehouse/-/issues/1
        data = bytes.fromhex(
            "10008000050094e6095cd89f590b23557340c2629612233cf09010a14183094260109c6866232080cc7f"
        )
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertEqual(item.code, "spr")
        self.assertEqual(str(item.stats[0]), "+13% Enhanced Damage")
        self.assertEqual(str(item.stats[2]), "+2 to Maximum Damage")

    def test_runeword_lore(self):
        # Lore: Ort Sol
        data = bytes.fromhex(
            "1008800405c055c637f1073af4558697412981070881506049e87f005516fb134582ff1000a0003500e07cbb001000a0003504e07c9800"
        )
        data, item = parse_item(data)
        self.assertEqual(data, b"")
        self.assertTrue(item.is_runeword)
        self.assertEqual(len(item.sockets), 2)
        item.sockets[0].print()
        item.sockets[1].print()
        self.assertEqual(item.sockets[0].code, "r09")
        self.assertEqual(item.sockets[1].code, "r12")
        rw = lookup_runeword(item.runeword_id)
        self.assertEqual(rw["name"], "Lore")
        for stat in item.stats:
            print(str(stat))
        self.assertEqual(str(item.stats[4]), "+1 to All Skills")  # runeword stat
        self.assertEqual(str(item.stats[2]), "+10 to Energy")  # runeword stat
        # TODO: sol rune stat -- should it be in representation?
        # self.assertEqual(str(item.sockets[1].stats[0]), "Lightning Resist 30%")
