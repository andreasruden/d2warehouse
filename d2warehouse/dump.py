# Copyright 2023 <omicron.me@protonmail.com>
# Copyright 2023 <andreasruden91@gmail.com>
#
# This file is part of d2warehouse.
#
# d2warehouse is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# d2warehouse is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Mercator. If not, see <https://www.gnu.org/licenses/>.
import argparse
from pathlib import Path
from d2warehouse.parser import parse_stash

from bitarray import bitarray


def txtbits(bits: bitarray) -> str:
    txt = "".join(str(b) for b in bits)
    grouped = [txt[i : i + 8] for i in range(0, len(txt), 8)]
    return " ".join(grouped)


def read_stash(path, verbose):
    with path.open("rb") as f:
        stash = parse_stash(f.read())

    print(f"Stash has {len(stash.tabs)} tabs")
    for i, tab in enumerate(stash.tabs):
        print(f" - tab {i}")
        print(f"    - {len(tab.items)} items")
        if verbose:
            print(f"    - {tab.item_data.hex()}")
        for j, item in enumerate(tab.items):
            item.print(with_raw=verbose)


def main():
    parser = argparse.ArgumentParser(
        prog="d2dump",
        description="dump a text description of the items in d2r shared stash files",
    )
    parser.add_argument("stashfile", type=Path)
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    read_stash(args.stashfile, args.verbose)
