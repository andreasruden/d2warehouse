# Copyright 2023 <omicron.me@protonmail.com>
# Copyright 2023 <andreasruden91@gmail.com>
#
# This file is part of d2warehouse.
#
# d2warehouse is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# d2warehouse is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Mercator. If not, see <https://www.gnu.org/licenses/>.
import json
import os
import re
from typing import Optional
from bitarray import bitarray
from dataclasses import dataclass
from enum import Enum

_data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
_basetype_map = None
_stats_map = None
_unique_map = None
_set_item_map = None
_runeword_map = None


class Quality(Enum):
    LOW = 1
    NORMAL = 2
    HIGH = 3
    MAGIC = 4
    SET = 5
    RARE = 6
    UNIQUE = 7
    CRAFTED = 8

    def __str__(self) -> str:
        return self.name.capitalize()


class LowQualityType(Enum):
    CRUDE = 0
    CRACKED = 1
    DAMAGED = 2
    LOW_QUALITY = 3

    def __str__(self) -> str:
        return self.name.capitalize()


@dataclass
class Stat:
    id: int | None = None  # TODO: These 3 should probably not be optional
    values: list[int] | None = None
    parameter: int | None = None
    text: str | None = None

    def print(self, indent=5):
        print(" " * indent, str(self))

    def __str__(self):
        subst_text = self.text
        for val in self.values:
            subst_text = subst_text.replace("#", str(val), 1)
        if self.parameter:
            subst_text = re.sub(r"\[[^\]]*\]", str(self.parameter), subst_text, 1)
        return subst_text


def txtbits(bits: bitarray) -> str:
    txt = "".join(str(b) for b in bits)
    grouped = [txt[i : i + 8] for i in range(0, len(txt), 8)]
    return " ".join(grouped)


@dataclass
class Item:
    raw_data: bytes
    is_identified: bool
    is_socketed: bool
    is_beginner: bool
    is_simple: bool
    is_ethereal: bool
    is_personalized: bool
    is_runeword: bool
    pos_x: int
    pos_y: int
    code: str
    uid: int | None = None
    lvl: int | None = None
    quality: Quality | None = None
    graphic: int | None = None
    implicit: int | None = None
    low_quality: LowQualityType | None = None
    prefixes: list[int] | None = None
    suffixes: list[int] | None = None
    set_id: int | None = None
    unique_id: int | None = None
    nameword1: int | None = None
    nameword2: int | None = None
    runeword_id: int | None = None
    personal_name: str | None = None
    defense: int | None = None
    durability: int | None = None
    max_durability: int | None = None
    sockets: list[Optional["Item"]] | None = None
    quantity: int | None = None
    stats: list[Stat] | None = None

    def print(self, indent=5, with_raw=False):
        properties = []
        base_name = lookup_basetype(self.code)["name"]
        print(" " * indent, f"{base_name} ({self.code})")
        if self.lvl:
            print(" " * indent, f"ilvl {self.lvl}")
        if self.is_simple:
            properties.append("Simple")
        else:
            properties.append("Extended")
        if self.is_ethereal:
            properties.append("Ethereal")
        if not self.is_identified:
            properties.append("Unidentified")
        if self.is_socketed:
            properties.append("Socketed")
        if self.is_runeword:
            properties.append("Runeword")
        if properties:
            print(" " * indent, ", ".join(properties))
        print(" " * indent, f"at {self.pos_x}, {self.pos_y}")
        if self.quality:
            print(" " * indent, self.quality)
        if self.prefixes:
            print(" " * indent, "Prefixes:", self.prefixes)
        if self.suffixes:
            print(" " * indent, "Suffixes:", self.suffixes)
        if self.set_id:
            itm = lookup_set_item(self.set_id)
            print(" " * indent, f"{itm['name']} ({self.set_id}), part of {itm['set']}")
        if self.unique_id:
            itm = lookup_unique(self.unique_id)
            print(" " * indent, f"{itm['name']} ({self.unique_id})")
        if self.runeword_id:
            rw = lookup_runeword(self.runeword_id)
            print(" " * indent, f"{rw['name']} runeword")
        if self.personal_name:
            print(" " * indent, f"Personal name: {self.personal_name}")
        if self.defense:
            print(" " * indent, f"Defense: {self.defense}")
        if self.durability is not None:
            print(
                " " * indent,
                f"Durability: {self.durability} out of {self.max_durability}",
            )
        if self.is_socketed:
            print(" " * indent, f"{len(self.sockets)} sockets:")
            for socket in self.sockets:
                if socket:
                    socket.print(indent + 4)
                else:
                    print(" " * (indent + 4), "Empty")
        if self.quantity:
            print(" " * indent, f"Quantity: {self.quantity}")
        if self.stats:
            print(" " * indent, "Stats:")
            for stat in self.stats:
                stat.print(indent + 4)
        if with_raw:
            print(" " * indent, "Raw Item Data:")
            bits = bitarray(endian="little")
            bits.frombytes(self.raw_data)
            print(" " * indent, txtbits(bits))
        print("")
        print("")


def lookup_basetype(code: str) -> dict:
    global _basetype_map
    if _basetype_map is None:
        with open(os.path.join(_data_path, "items.json")) as f:
            _basetype_map = json.load(f)
    return _basetype_map[code]


def lookup_stat(id: int) -> dict:
    global _stats_map
    if _stats_map is None:
        with open(os.path.join(_data_path, "stats.json")) as f:
            _stats_map = json.load(f)
    return _stats_map[str(id)]


def lookup_unique(id: int) -> dict:
    global _unique_map
    if _unique_map is None:
        with open(os.path.join(_data_path, "uniques.json")) as f:
            _unique_map = json.load(f)
    return _unique_map[str(id)]


def lookup_set_item(id: int) -> dict:
    global _set_item_map
    if _set_item_map is None:
        with open(os.path.join(_data_path, "sets.json")) as f:
            _set_item_map = json.load(f)
    return _set_item_map[str(id)]


def lookup_runeword(id: int) -> dict:
    global _runeword_map
    if _runeword_map is None:
        with open(os.path.join(_data_path, "runewords.json")) as f:
            _runeword_map = json.load(f)
    return _runeword_map[str(id)]
