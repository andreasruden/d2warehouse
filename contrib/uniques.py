import csv
import json
import os
import sys

path = sys.argv[1] if len(sys.argv) >= 2 else "."

category = "Base"
setitems = {}
with open(os.path.join(path, "setitems.txt")) as f:
    dr = csv.DictReader(f, delimiter="\t")
    for row in dr:
        if row["index"] == "Expansion":
            category = row["index"]
            continue
        setitems[row["*ID"]] = {
            "name": row["index"],
            "set": row["set"],
            "itembase": row["item"],
            "req_lvl": int(row["lvl req"]),
            "ilvl": int(row["lvl"]),
            "rarity": int(row["rarity"]),
            "category": category,
        }

category = "Base"
uniqueitems = {}
with open(os.path.join(path, "uniqueitems.txt")) as f:
    dr = csv.DictReader(f, delimiter="\t")
    for row in dr:
        if row["index"] in [
            "Expansion",
            "Armor",
            "Elite Uniques",
            "Rings",
            "Class Specific",
        ]:
            category = row["index"]
            continue
        if row["*ID"] == "288":
            category = "Patch 1.10+"
        if len(row["lvl req"]) == 0:
            continue  # deleted uniques
        uniqueitems[row["*ID"]] = {
            "name": row["index"],
            "itembase": row["code"],
            "req_lvl": int(row["lvl req"]),
            "ilvl": int(row["lvl"]),
            "rarity": int(row["rarity"]),
            "category": category,
        }

with open("uniques.json", "w", newline="\n") as f:
    json.dump(uniqueitems, f, indent=4)
    f.write("\n")

with open("sets.json", "w", newline="\n") as f:
    json.dump(setitems, f, indent=4)
    f.write("\n")
