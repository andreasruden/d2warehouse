import csv
import json
import os
import sys


path = sys.argv[1] if len(sys.argv) >= 2 else "."

runewords = {}
id = 0
with open(os.path.join(path, "runes.txt")) as f:
    dr = csv.DictReader(f, delimiter="\t")
    for row in dr:
        id += 1
        if row["complete"] != "1":
            continue
        runewords[id + 26] = {
            "name": row["*Rune Name"],
            "itembases": [
                row[f"itype{i}"] for i in range(1, 7) if len(row[f"itype{i}"]) > 0
            ],
            "runes": [row[f"Rune{i}"] for i in range(1, 7) if len(row[f"Rune{i}"]) > 0],
        }

with open("runewords.json", "w", newline="\n") as f:
    json.dump(runewords, f, indent=4)
    f.write("\n")
